package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie1a {
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int parzyste=0,nieparzyste=0;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<n;i++)
        {
            if(tab[i]%2==0){parzyste++;}
            else  {nieparzyste++;}
            System.out.println(tab[i]);
        }
        System.out.println("parzyste: "+ parzyste);
        System.out.println("nieparzyste: "+ nieparzyste);
    }
}
