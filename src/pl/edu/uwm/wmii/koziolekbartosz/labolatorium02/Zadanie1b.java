package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie1b {
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int ujemne=0,dodatnie=0,zerowe=0;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<n;i++)
        {
            if(tab[i]>0){dodatnie++;}
            if(tab[i]<0) {ujemne++;}
            if(tab[i]==0){zerowe++;}
            System.out.println(tab[i]);
        }
        System.out.println("dodatnie: "+ dodatnie);
        System.out.println("ujemne: "+ ujemne);
        System.out.println("zerowe: "+ zerowe);
    }
}
