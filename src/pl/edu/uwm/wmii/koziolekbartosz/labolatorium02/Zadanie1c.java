package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie1c {
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int max=-999,suma=0;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
            if(tab[i]>max){max=tab[i];}
        }
        for(int i=0;i<n;i++)
        {
            if(tab[i]==max)suma++;
            System.out.println(tab[i]);
        }
        System.out.println("największa liczba: "+ max);
        System.out.println("wystepuje: "+ suma+" razy");
    }
}
