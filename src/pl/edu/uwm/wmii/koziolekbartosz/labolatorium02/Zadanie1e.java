package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie1e {
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int max=0;
        int licznik=0;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<n;i++) {
            if(tab[i]>0)licznik++;
            if(tab[i]<=0){
                if(licznik>max)max=licznik;
                licznik=0;
            }
            System.out.println(tab[i]);
        }
        System.out.println("najdluzszy ciag dodatnich liczb ma dlugosc: "+ max);
    }
}
