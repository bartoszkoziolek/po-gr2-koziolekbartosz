package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie2b {

    public static int ileDodatnich(int tab[])
    {
        int temp=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0)temp++;
        }
        return temp;
    }
    public static int ileUjemnych(int tab[])
    {
        int temp=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]<0)temp++;
        }
        return temp;
    }
    public static int ileZerowych(int tab[])
    {
        int temp=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]==0)temp++;
        }
        return temp;
    }

    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int ujemne,dodatnie,zerowe;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<n;i++)
        {

            System.out.println(tab[i]);
        }
        System.out.println("dodatnie: "+ ileDodatnich(tab));
        System.out.println("ujemne: "+ ileUjemnych(tab));
        System.out.println("zerowe: "+ ileZerowych(tab));
    }
}
