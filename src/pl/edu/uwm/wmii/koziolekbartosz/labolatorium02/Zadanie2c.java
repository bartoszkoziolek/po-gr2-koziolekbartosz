package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie2c {
    public static int ileMaksymalnych(int tab[])
    {
        int max=-999;
        int licznik=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>max)max=tab[i];
        }
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]==max)licznik++;
        }
        return licznik;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int max=-999,suma=0;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<n;i++)
        {
            System.out.println(tab[i]);
        }
        System.out.println("wystepuje: "+ ileMaksymalnych(tab)+" razy");
    }
}
