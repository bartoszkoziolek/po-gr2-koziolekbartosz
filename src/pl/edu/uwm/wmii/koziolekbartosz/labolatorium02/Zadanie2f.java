package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie2f {
    public static void signum(int tab[])
    {
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0)tab[i]=1;
            else tab[i]=-1;
        }
    }

    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        signum(tab);
        for(int i=0;i<n;i++)
        {
            System.out.println(tab[i]);
        }

    }
}
