package pl.edu.uwm.wmii.koziolekbartosz.labolatorium02;
import java.util.Random;
import java.util.Scanner;
public class Zadanie2g {
public static void odwrocfragment(int tab[],int lewy,int prawy)
{
    int l=lewy,p=prawy;
    int temp;
    while(l<p)
    {
        temp=tab[lewy];
        tab[lewy]=tab[prawy];
        tab[prawy]=temp;
        l++;
        p--;
    }
}

    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        int n=odczyt.nextInt();
        int lewy,prawy;
        int temp;
        Random generator = new Random();

        int[] tab = new int[n];
        for(int i=0;i<n;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<n;i++)
        {
            System.out.println(tab[i]);
        }
        System.out.println("podaj przedzial do odwrocenia");
        lewy=odczyt.nextInt();
        prawy=odczyt.nextInt();
        odwrocfragment(tab,lewy,prawy);
        for(int i=0;i<n;i++)
        {
            System.out.println(tab[i]);
        }
    }
}
