package pl.edu.uwm.wmii.koziolekbartosz.labolatorium03;

public class Zadanie1a {
    public static int wystapienie_litera(String napis, char litera)
    {
        int ile_liter = 0;
        char znak_z_napisu;
        for (int i = 0 ; i < napis.length() ; i++)
        {
            znak_z_napisu = napis.charAt(i);
            if(znak_z_napisu == litera)
            {
                ile_liter++;
            }
        }
        return ile_liter;
    }
    public static void main(String[] args)
    {
        String przykladowy_napis = "adgcrtcndfc";
        System.out.println(wystapienie_litera(przykladowy_napis, 'c'));

    }
}
