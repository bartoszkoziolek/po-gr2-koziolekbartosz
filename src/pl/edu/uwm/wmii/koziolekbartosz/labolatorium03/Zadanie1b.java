package pl.edu.uwm.wmii.koziolekbartosz.labolatorium03;

public class Zadanie1b {
    public static int countSubStr(String napis, String subnapis){
        int count = 0;
        int sequence = 0;
        int length = subnapis.length();

        for (int i = 0; i < napis.length(); i++){
            if (napis.charAt(i) == subnapis.charAt(sequence)){
                sequence++;
            } else {
                sequence = 0;
            }
            if (sequence == length) {
                count++;
                sequence = 0;
            }
        }
        return count;
    }
    public static void main(String[] args)
    {

        String przykladowy_napis = "wolneawolne";
        String subnapis="wolne";
        System.out.println(countSubStr(przykladowy_napis, subnapis));
    }

}
