package pl.edu.uwm.wmii.koziolekbartosz.labolatorium03;

import java.util.Scanner;

public class Zadanie1c {
    public static String middle(String string){
        String wynik = "";
        int srodek = string.length() / 2;
        if (string.length() % 2 == 1){
            wynik += string.charAt(srodek);
        } else {
            wynik += string.charAt(srodek - 1);
            wynik += string.charAt(srodek);

        }
        return wynik;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);

        String napis = odczyt.nextLine();

        System.out.println(middle(napis));
    }
}
