package pl.edu.uwm.wmii.koziolekbartosz.labolatorium03;

import java.util.Scanner;

public class Zadanie1d {
    public static String repeat(String wyraz, int n){
        String napis="";
    for(int i=0;i<n;i++)
    {
        napis+=wyraz;
    }
        return napis;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);

        String napis = odczyt.nextLine();

        System.out.println(repeat(napis,5));
    }
}
