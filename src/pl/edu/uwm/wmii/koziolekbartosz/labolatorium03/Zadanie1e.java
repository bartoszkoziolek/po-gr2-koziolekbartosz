package pl.edu.uwm.wmii.koziolekbartosz.labolatorium03;

import java.util.Scanner;

public class Zadanie1e {


    public static int[] where(String str, String subStr)
    {
        int[] tmp = new int[str.length()];
        int licznik=0;
        for(int i=0;i<str.length()-subStr.length()+1;i++) {
            if(str.substring(i,i+subStr.length()).compareTo(subStr)==0)
                tmp[i]=1;
        }
        for(int i=0;i<tmp.length;i++){
            licznik=(tmp[i]==1)?++licznik:licznik;
        }
        int[] wynik= new int[licznik];
        int j=0;
        for(int i=0;i<str.length();i++){
            if(tmp[i]==1){
                wynik[j]=i;
                j++;
            }
        }
        return wynik;
    }

    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);

        String napis = odczyt.nextLine();
        String podnapis = odczyt.nextLine();

        int[]wynik=where(napis,podnapis);
        for(int i=0;i<wynik.length;i++)
        {
            System.out.println(wynik[i]);
        }
    }
}
