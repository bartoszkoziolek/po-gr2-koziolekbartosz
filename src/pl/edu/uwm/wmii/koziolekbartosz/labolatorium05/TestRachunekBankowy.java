package pl.edu.uwm.wmii.koziolekbartosz.labolatorium05;

public class TestRachunekBankowy {

    public static void main(String[] args)
    {
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();
        saver1.ustawSaldo(2000);
        saver2.ustawSaldo(3000);
        saver1.setRocznaStopaProcentowa(0.04);
        saver2.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.wyswietlSlado();
        saver2.wyswietlSlado();
        saver1.setRocznaStopaProcentowa(0.05);
        saver2.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.wyswietlSlado();
        saver2.wyswietlSlado();
    }
}
