package pl.edu.uwm.wmii.koziolekbartosz.labolatorium08;
import java.time.LocalDate;
import pl.imiajd.koziolek.Osoba1;
import pl.imiajd.koziolek.Pracownik;
import pl.imiajd.koziolek.Student1;
public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba1[] ludzie = new Osoba1[2];
        ludzie[0] = new Pracownik("Kowalski","Jan",LocalDate.now(),true,5000,LocalDate.now());
        ludzie[1] = new Student1("Nowak","Małgorzata",LocalDate.now(),false, "informatyka");
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba1 p : ludzie) {
            System.out.println(p.getNazwisko() +" " + p.getImiona() + " " +p.getDataurodzenia()+" " + p.getPlec()+ ": " + p.getOpis());
        }
    }
}