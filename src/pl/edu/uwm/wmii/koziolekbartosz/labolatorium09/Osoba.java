package pl.edu.uwm.wmii.koziolekbartosz.labolatorium09;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;
public class Osoba implements Comparable<Osoba> {


    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String toString() {
        return String.format("nazwisko: " + "%s"  + ", data Urodzenia: " + "%04d-%02d-%02d" , this.getNazwisko(), this.getDataUrodzenia().getYear(),
                this.getDataUrodzenia().getMonthValue(), this.getDataUrodzenia().getDayOfMonth());
    }

    public boolean equals(Object ziomek) {
        if (this == ziomek)
            return true;
        if (ziomek == null || getClass() != ziomek.getClass())
            return false;
        Osoba osoba = (Osoba) ziomek;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    public int compareTo(Osoba ziomek) {
        if (this.nazwisko != null && ziomek.nazwisko != null) {
            return this.nazwisko.compareTo(ziomek.nazwisko);
        }
        if (this.dataUrodzenia != null && ziomek.dataUrodzenia != null) {
            return this.dataUrodzenia.compareTo(ziomek.dataUrodzenia);
        }
        return 0;
    }



    public int ileLat() {
        LocalDate ur = this.dataUrodzenia;
        LocalDate data = LocalDate.now();
        if (ur != null)
        {
            return Period.between(ur, data).getYears();
        } else {
            return 0;
        }
    }
    public int ileMiesiecy() {
        LocalDate ur = this.dataUrodzenia;
        LocalDate data = LocalDate.now();
        if (ur != null)
        {
            return Period.between(ur, data).getMonths();
        } else {
            return 0;
        }
    }
    public int ileDni() {
        LocalDate ur = this.dataUrodzenia;
        LocalDate data = LocalDate.now();
        if (ur != null)
        {
            return Period.between(ur, data).getDays();
        } else {
            return 0;
        }
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;
}
