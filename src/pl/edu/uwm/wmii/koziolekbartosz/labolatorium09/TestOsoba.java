package pl.edu.uwm.wmii.koziolekbartosz.labolatorium09;
import java.time.LocalDate;
import java.util.Arrays;
public class TestOsoba {
    public static void main(String[] args) {
        Osoba grupa[] = new Osoba[5];
        grupa[0] = new Osoba("Koziołek", LocalDate.of(1998, 7, 8));
        grupa[1] = new Osoba("Rodak", LocalDate.of(1987, 3, 5));
        grupa[2] = new Osoba("Rodak", LocalDate.of(1995, 12, 1));
        grupa[3] = new Osoba("Pawluczuk", LocalDate.of(1996, 1, 12));
        grupa[4] = new Osoba("Żebrowska", LocalDate.of(1996, 1, 12));
        for (Osoba i : grupa) {
            System.out.println(i.toString());
        }
        System.out.println(grupa[1].equals(grupa[2]));
        Arrays.sort(grupa);
        for (Osoba i : grupa) {
            System.out.println(i.toString());
            System.out.println("Lat: " + i.ileLat() + ", Miesięcy: " + i.ileMiesiecy()+ ", Dni: " + i.ileDni());
        }
    }
}