package pl.edu.uwm.wmii.koziolekbartosz.labolatorium10;
import java.time.LocalDate;
import java.util.Objects;
public class Osoba implements Comparable<Osoba>{


    Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public String toString() {
        return String.format("nazwisko: " + "%s"  + ", dataUrodzenia: " + "%04d-%02d-%02d" , this.getNazwisko(), this.getDataUrodzenia().getYear(),
                this.getDataUrodzenia().getMonthValue(), this.getDataUrodzenia().getDayOfMonth());
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    public int compareTo(Osoba o) {
        if (this.nazwisko != null && o.nazwisko != null) {
            return this.nazwisko.compareTo(o.nazwisko);
        }
        if (this.dataUrodzenia != null && o.dataUrodzenia != null) {
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        return 0;
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;

}
