package pl.edu.uwm.wmii.koziolekbartosz.labolatorium10;
import java.time.LocalDate;

class Student extends Osoba implements Comparable<Osoba>, Cloneable
{

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }
    public int compareTo(Osoba ziomek)
    {
        if(this.getNazwisko()!= null && ziomek.getNazwisko() != null)
        {
            return this.getNazwisko().compareTo(ziomek.getNazwisko());
        }
        if(this.getDataUrodzenia() != null && ziomek.getDataUrodzenia() != null)
        {
            return this.getDataUrodzenia().compareTo(ziomek.getDataUrodzenia());
        }
        return 0;
    }
    public double getSredniaOcen()
    {
        return sredniaOcen;
    }

    public String toString()
    {
        return String.format("nazwisko: " + "%s"  + ", dataUrodzenia: " + "%04d-%02d-%02d" + ", średnia ocen: " + "%.2f",

                super.getNazwisko(), super.getDataUrodzenia().getYear(), super.getDataUrodzenia().getMonthValue(),

                super.getDataUrodzenia().getDayOfMonth(), this.getSredniaOcen());
    }
    private double sredniaOcen;
}
