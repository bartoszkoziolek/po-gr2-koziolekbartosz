package pl.edu.uwm.wmii.koziolekbartosz.labolatorium10;
import java.time.LocalDate;
import java.util.Arrays;
public class TestOsoba {
    public static void main(String[] args) {
        Osoba grupa[] = new Osoba[5];
        grupa[0] = new Osoba("Koziołek", LocalDate.of(1998, 7, 8));
        grupa[1] = new Osoba("Rodak", LocalDate.of(1986, 3, 5));
        grupa[2] = new Osoba("Rodak", LocalDate.of(1990, 10, 10));
        grupa[3] = new Osoba("Pawluczuk", LocalDate.of(1997, 4, 21));
        grupa[4] = new Osoba("Nowacka", LocalDate.of(1997, 4, 21));

        for (Osoba ziomek : grupa) {
            System.out.println(ziomek.toString());
        }


        System.out.println(grupa[1].equals(grupa[2]));

        Arrays.sort(grupa);

        for (Osoba ziomek : grupa) {
            System.out.println(ziomek.toString());
        }
    }
}
