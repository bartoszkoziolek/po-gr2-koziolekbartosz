package pl.edu.uwm.wmii.koziolekbartosz.labolatorium10;
import java.time.LocalDate;
import java.util.Arrays;
public class TestStudent {
    public static void main(String[] args)
    {
        Student grupa[] = new Student[5];

        grupa[0] = new Student("Koziołek", LocalDate.of(1998, 7, 9), 4.2);
        grupa[1] = new Student("Rodak", LocalDate.of(1987, 3, 5), 4.1);
        grupa[2] = new Student("Rodak", LocalDate.of(1990, 12, 1),4.6);
        grupa[3] = new Student("Pawluczuk", LocalDate.of(1993, 7, 24), 3.9);
        grupa[4] = new Student("Nowacka", LocalDate.of(1993, 7, 24),3.5);

        for (Student ziomek : grupa) {
            System.out.println(ziomek.toString());
        }

        System.out.println(grupa[1].equals(grupa[2]));

        Arrays.sort(grupa);

        for (Student ziomek : grupa) {
            System.out.println(ziomek.toString());
        }
    }
}