package pl.edu.uwm.wmii.koziolekbartosz.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Rodzimy się.. lecz po co?\n" +
                "tak naprawdę nikt nie zna celu.\n" +
                "Umieramy... zwykle nocą.\n" +
                "lecz by nie skończyc tak jak wielu\n" +
                "innych, szukamy powodu, tego co nie znamy.\n" +
                "Dlaczego sie rodzimy, dlaczego umieramy.");

    }
}
