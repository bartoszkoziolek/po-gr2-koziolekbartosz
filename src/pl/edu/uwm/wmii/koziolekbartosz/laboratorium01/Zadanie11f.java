package pl.edu.uwm.wmii.koziolekbartosz.laboratorium01;

import java.util.Scanner;

public class Zadanie11f {
    public static void main(String[] args) {
        int n;
        int wynik = 0;
        int liczba;
        Scanner odczyt = new Scanner(System.in); //obiekt do odebrania danych od użytkownika
        n = odczyt.nextInt();
        for (int i = 0; i < n; i++) {
            liczba = odczyt.nextInt();
            wynik += liczba * liczba;
        }
        System.out.println(wynik);
    }
}
