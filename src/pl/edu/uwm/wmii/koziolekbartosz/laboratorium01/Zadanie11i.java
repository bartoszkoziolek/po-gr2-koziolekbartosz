package pl.edu.uwm.wmii.koziolekbartosz.laboratorium01;

import java.util.Scanner;

public class Zadanie11i {
    public static void main(String[] args) {
        int n;
        double wynik=0;
        int liczba;
        int te;

        Scanner odczyt = new Scanner(System.in); //obiekt do odebrania danych od użytkownika
        n = odczyt.nextInt();
        for(int i=0;i<n;i++)
        {
            liczba = odczyt.nextInt();
            te=liczba;
            int tr=i+1;
            int silnia=1;
            while (tr > 1)
            {
                silnia = silnia *tr;
                 tr-= 1;
            }

            if(i%2==0)wynik -=te/silnia;
            else wynik+=te/silnia;
        }
        System.out.println(wynik);
    }
}
