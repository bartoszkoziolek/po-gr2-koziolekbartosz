package pl.edu.uwm.wmii.koziolekbartosz.laboratorium01;

import java.util.Scanner;

public class Zadanie21d {
    public static void main(String[] args) {
        int n;
        int wynik=0;
        int liczba;

        Scanner odczyt = new Scanner(System.in); //obiekt do odebrania danych od użytkownika
        n = odczyt.nextInt();
        int[] tablica=new int[n];
        for(int i=0;i<n;i++)
        {
            liczba = odczyt.nextInt();
            tablica[i]=liczba;
        }
        for(int i=1;i<n-1;i++)
        {
            if(tablica[i]<(tablica[i-1]+tablica[i+1])/2)wynik+=1;
        }
        System.out.println(wynik);
    }
}
