package pl.edu.uwm.wmii.koziolekbartosz.laboratorium06;

import pl.imiajd.koziolek.Adres;

public class TestZadanie2 {
    public static void main(String[] args) {
        Adres adres1 = new Adres("Warszawska", 10, "Olsztyn", 11042);
        Adres adres2 = new Adres("Nowa", 11, 12, "Olsztyn", 11042);
        adres1.pokaz();
        adres2.pokaz();
    }
}
