package pl.imiajd.koziolek;

   public class Adres {
        public Adres(String ul, int nd, int nm, String mi, int kod) {
            ulica = ul;
            numer_domu = nd;
            numer_mieszkania = nm;
            miasto = mi;
            kod_pocztowy = kod;
        }

        public Adres(String ul, int nd, String mi, int kod) {
            ulica = ul;
            numer_domu = nd;
            miasto = mi;
            kod_pocztowy = kod;
        }
        public void pokaz() {
            System.out.println(kod_pocztowy + " " + miasto);
            if (numer_mieszkania == 0) {
                System.out.println("ul. " + ulica + " " + numer_domu);
            } else {
                System.out.println("ul. " + ulica + " " + numer_domu + "/" + numer_mieszkania);
            }
        }

        private String ulica;
        private int numer_domu;
        private int numer_mieszkania = 0;
        private String miasto;
        private int kod_pocztowy;
    }


