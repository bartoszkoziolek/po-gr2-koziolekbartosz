package pl.imiajd.koziolek;
import java.time.LocalDate;
public abstract class Osoba1 {
    public Osoba1(String nazwisko, String imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.plec = plec;
        this.dataUrodzenia = dataUrodzenia;
    }

    public abstract  String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona()
    {
        return imiona;
    }
    public LocalDate getDataurodzenia()
    {
        return dataUrodzenia;
    }
    public String getPlec()
    {
        if(plec=true)return "mężczyzna";
        else return "kobieta";
    }


    private String nazwisko;
    String imiona;
    LocalDate dataUrodzenia;
    boolean plec;
}
