package pl.imiajd.koziolek;

import java.time.LocalDate;
public class Pracownik extends Osoba1
{
    public Pracownik(String nazwisko,String imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia= dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }
    public LocalDate getDataZatrudnienia()
    {
        return dataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją: "+getPobory() +"," + "zatrudniony dnia: " + getDataZatrudnienia());
    }

    private double pobory;
    LocalDate dataZatrudnienia;
}