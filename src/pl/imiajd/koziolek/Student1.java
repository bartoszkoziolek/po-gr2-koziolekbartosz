package pl.imiajd.koziolek;

import java.time.LocalDate;

 public class Student1 extends Osoba1
{
    public Student1(String nazwisko, String imiona, LocalDate dataUrodzenia, boolean plec, String kierunek)
    {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.kierunek = kierunek;
    }

    public String getOpis()
    {
        return String.format("student na kierunku:  %s, ze srednią równą: %.2f", kierunek, sredniaOcen);
    }
    public double getSrednia()
    {
        return sredniaOcen;
    }

    public void setSrednia(double srednia)
    {
        sredniaOcen=srednia;
    }

    private String kierunek;
    double sredniaOcen;
}
